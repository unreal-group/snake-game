// Fill out your copyright notice in the Description page of Project Settings.


#include "SnakeBase.h"
#include "SnakeElementBase.h"
#include "Interactable.h"
#include "Kismet/KismetSystemLibrary.h"

// Sets default values
ASnakeBase::ASnakeBase()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	ElementSize = 100.f;
	CurrentMovementDirection = EMovementDirection::DOWN;
}

// Called when the game starts or when spawned
void ASnakeBase::BeginPlay()
{
	Super::BeginPlay();
	SetActorTickInterval(MovementSpeed);
	AddSnakeElement(4);
}

// Called every frame
void ASnakeBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	Move();
}

void ASnakeBase::AddSnakeElement(const int ElementsCount)
{
	for (int i = 0; i < ElementsCount; i++)
	{
		const FVector NewLocation(SnakeElements.Num() * ElementSize, 0, 0);
		const FTransform NewTransform(NewLocation);
		auto NewSnakeElement = GetWorld()->SpawnActor<ASnakeElementBase>(SnakeElementClass, NewTransform);
		NewSnakeElement->SetActorHiddenInGame(true);
		NewSnakeElement->AttachOwner(this);
		const int32 ElemIndex = SnakeElements.Add(NewSnakeElement);
		if (ElemIndex == 0)
		{
			NewSnakeElement->SetFirstElementType();
		}
	}
}

void ASnakeBase::Move()
{
	TrySwitchDirection(CurrentMovementDirection);

	FVector MovementDirection(ForceInitToZero);
	const auto Movement = ElementSize;

	switch (CurrentMovementDirection)
	{
	case EMovementDirection::UP:
		MovementDirection.X += Movement;
		break;
	case EMovementDirection::DOWN:
		MovementDirection.X -= Movement;
		break;
	case EMovementDirection::LEFT:
		MovementDirection.Y += Movement;
		break;
	case EMovementDirection::RIGHT:
		MovementDirection.Y -= Movement;
		break;
	}

	SnakeElements[0]->ToggleCollision();

	for (auto i = SnakeElements.Num() - 1; i > 0; i--)
	{
		SnakeElements[i]->SetActorHiddenInGame(false);
		auto CurrentElement = SnakeElements[i];
		auto PreviousElement = SnakeElements[i - 1];
		auto PreviousPosition = PreviousElement->GetActorLocation();
		CurrentElement->SetActorLocation(PreviousPosition);
	}

	SnakeElements[0]->SetActorHiddenInGame(false);
	SnakeElements[0]->AddActorWorldOffset(MovementDirection);
	SnakeElements[0]->ToggleCollision();
}

void ASnakeBase::SnakeElementOverlap(ASnakeElementBase* OverlappedElement, AActor* Other)
{
	if (IsValid(OverlappedElement))
	{
		IInteractable* Interactable = Cast<IInteractable>(Other);
		if (Interactable)
		{
			int32 ElemIndex;
			SnakeElements.Find(OverlappedElement, ElemIndex);
			Interactable->Interact(this, ElemIndex == 0);
		}
	}
}

void ASnakeBase::IncreaseSpeed(const float SpeedDelta)
{
	const auto TickInterval = GetActorTickInterval() * (1 - SpeedDelta);
	SetActorTickInterval(TickInterval);
}

void ASnakeBase::TrySwitchDirection(EMovementDirection& Current)
{
	bool MovementValid;

	switch (Current)
	{
	case EMovementDirection::UP:
		MovementValid = TempMovementDirection != EMovementDirection::DOWN;
		break;
	case EMovementDirection::DOWN:
		MovementValid = TempMovementDirection != EMovementDirection::UP;
		break;
	case EMovementDirection::LEFT:
		MovementValid = TempMovementDirection != EMovementDirection::RIGHT;
		break;
	case EMovementDirection::RIGHT:
		MovementValid = TempMovementDirection != EMovementDirection::LEFT;
		break;
	default:
		MovementValid = false;
	}

	if (MovementValid)
	{
		Current = TempMovementDirection;
	}
}
